<?php
namespace Drupal\sendpulse8x\Libs;

include_once __DIR__ . '/sendpulse/autoload.php';

use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

class SendPulseAPI{
	public function load($user_id, $secret){
		try{
			$obj = new ApiClient($user_id, $secret);
			return $obj;
		}catch(Exception $e){
			return $e->getTrace();
		}
	}
}