<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9212e8ed12f8c22f9f9fddfc48817bfc
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Sendpulse\\RestApi\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Sendpulse\\RestApi\\' => 
        array (
            0 => __DIR__ . '/..' . '/sendpulse/rest-api/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9212e8ed12f8c22f9f9fddfc48817bfc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9212e8ed12f8c22f9f9fddfc48817bfc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
