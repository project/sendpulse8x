<?php
namespace Drupal\sendpulse8x\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Implements an sendpulse8x form.
 */
class SendPulseForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sendpulse8x_variables_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sendpulse8x.sendpulse8x_variables',
      'sendpulse8x.sendpulse8x_roles',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configRoles = $this->config('sendpulse8x.sendpulse8x_roles');
    $configConnect = $this->config('sendpulse8x.sendpulse8x_variables');
    
	$role_objects = Role::loadMultiple();
	$system_roles = array_combine(array_keys($role_objects), array_map(function($a){ return $a->label();}, $role_objects));
	
	$form['user_id'] = [  
      '#type' => 'textfield',  
      '#title' => $this->t('API User ID'),  
      '#description' => $this->t('API User ID'),  
      '#default_value' => $configConnect->get('user_id'),  
    ];
	
	$form['secret'] = [  
      '#type' => 'textfield',  
      '#title' => $this->t('API Secret'),  
      '#description' => $this->t('API Secret'),  
      '#default_value' => $configConnect->get('secret'),  
    ];	
	
	$form['button'] = [
		'#type' => 'button',
		'#value' => $this->t('Test Connection'),
		'#ajax' => [
			'callback' => [$this, '_sp_reading_config_connect'],
			'event' => 'click',
		],
	];
	
	$form['api_token_verfiy_msg'] = [
      '#markup' => '<div class="token_verfiy_msg"></div>',
      '#allowed_tags' => ['div']
    ];
	
	$form['contact_list_select'] = [
      '#markup' => '<div class="list_options"></div>',
      '#allowed_tags' => ['div']
    ];
	
	$form['UserRoles'] = [
		'#type' => 'checkboxes',
		'#options' => $system_roles,
		'#title' => $this->t('Which role(s) you would like to sync with Send Pulse at time of registration?'),
		'#default_value' => $configRoles->get('UserRoles'),  
	];
	
	if(!empty($configRoles->get('sp_contact_list'))){
		$list_array = [];
		$form['sp_contact_list'] = [
			'#type' => 'checkboxes',
			'#options' => $list_array,
			'#title' => $this->t('Select list to import contact'),
			'#default_value' => $configRoles->get('sp_contact_list'),  
		];
	}
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save config.
    $this->config('sendpulse8x.sendpulse8x_variables')
      ->set('user_id', $form_state->getValue('user_id'))
      ->set('secret', $form_state->getValue('secret'))
      ->set('sp_contact_list', $form_state->getValue('sp_contact_list'))
      ->save();
	  
    $this->config('sendpulse8x.sendpulse8x_roles')
      ->set('UserRoles', $form_state->getValue('UserRoles'))
      ->save();

    parent::submitForm($form, $form_state);
  }
  
  public function _sp_reading_config_connect(array &$form, FormStateInterface $form_state){
	$response = new AjaxResponse();
	
	$marketo_object = new \Drupal\sendpulse8x\Controller\SendPulseAPIController();
	$marketo_object->user_id = \Drupal::request()->get('user_id');
	$marketo_object->secret = \Drupal::request()->get('secret');
	$marketo_object->api_obj = $marketo_object->load();
	$res = $marketo_object->testConnection();
	if(isset($res->error)){
		$token = ['valid' => 0, 'message' => $res->error_description];
	}else{
		$token = ['valid' => 1, 'message' => 'Connected'];
		$form['sp_contact_list'] = [
			'#type' => 'select',
			'#title' => $this->t('Select list to import contact'),
			'#default_value' => '',
			'#options' => [],
		];
		$response->addCommand(new ReplaceCommand('.list_options', ($form['sp_contact_list'])));
	}
    if ($token['valid'] == 1) {
      $error_class = "messages--status";
    }
    else {
      $error_class = "messages--error";
    }
	$response->addCommand(new HtmlCommand('.token_verfiy_msg', '<div class="messages '.$error_class.'">' . $token['message'] . '</div>'));
	
    return $response;
  }
}