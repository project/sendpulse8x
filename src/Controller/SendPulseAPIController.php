<?php

/**
 * @file
 * Contains \Drupal\sendpulse8x\Controller\SendPulseAPIController
 */

namespace Drupal\sendpulse8x\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\sendpulse8x\Libs\SendPulseAPI;


class SendPulseAPIController extends ControllerBase {
	public $user_id;
	public $secret;
	public $email;
	public $api_obj;
	
	public function __construct() {
		$this->sendObj = new SendPulseAPI();
	}
	
	public function load() {
		return $this->sendObj->load($this->user_id, $this->secret);
	}
	
	public function testConnection(){
		return $this->api_obj->listAddressBooks();
	}
	
	public function createLeadinSendPulse($email, $bookID = ''){
		$emails = [
			[
				'email' => $email,
				'variables' => [
					/* 'phone' => '+12345678900',
					'name' => 'User Name', */
				]
			]
		];
		return $this->api_obj->addEmails($bookID, $emails);
	}
}
